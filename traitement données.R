rm(list=ls())

library(readxl)
##Traitement base de données sur les résultats à la présidentielle 1er tour 2017
resultats_pres <- read_excel("Presidentielle_2017_Resultats_Communes_Tour_1_c.xls", col_names = TRUE)
resultats_pres <- resultats_pres[!(is.na(resultats_pres$`Code du département`)),]
resultats_pres_35 <- resultats_pres[resultats_pres$`Code du département` == 35,]
write.csv(resultats_pres_35, file  = "resultats_presidentielle_1er_tour_35.csv")

library(readr)
resultats_pres_35 <- read_csv("resultats_presidentielle_1er_tour_35.csv", col_select = c(2:96))
particip35 <- resultats_pres_35[,1:9]
particip35 <- read_csv("participation_35.csv", col_select = c(2:10))


##Traitement base de donnée INSEE sur 35
data <- read_excel("data.xlsx", skip = 1)

##Fusion de communes entre l'élection 2017 et les données INSEE
# Baillé (11) + Saint-Marc-le-Blanc (292) -> Saint-Marc-le-Blanc (35292)
# Chancé (53) + Piré-sur-Seiche (220) -> Piré-Chancé (35220)
# Dompierre-du-Chemin (100) + Luitré (163) -> Luitré-Dompierre (35163)
# La Fontenelle (113) + Antrain (4) + Saint-Ouen-la-Rouërie (303) + Tremblay (341) -> Val-Couesnon (35004)
# Lanhélin (147) + Saint-Pierre-de-Plesguen (308) + Tressé (344) -> Mesnil-Roc'h (35308)
# Saint-Georges-de-Chesné (269) + Saint-Jean-sur-Couesnon (282) + Saint-Marc-sur-Couesnon (293) + Vendel (348) -> Rives-du-Couesnon (35282)
# Saint-M'Hervon (301) + Montauban-de-Bretagne (184) -> Montauban-de-Bretagne (35184)

anciennes_communes <- list(c(11,292), c(53,220), c(100,163), c(113,4,303,341), c(147,308,344), c(269,282,293,348), c(301,184))
communes_disparues <- c(11, 53, 100, 113, 303, 341, 147, 344, 269, 293, 348, 301)
index_cd <- match(communes_disparues, particip35$`Code de la commune`)
nouvelles_communes <- c(292, 220, 163, 4, 308, 282, 184)
index_nc <- match(nouvelles_communes, particip35$`Code de la commune`)
for (i in 1:7){
  index_temp <- match(anciennes_communes[i][[1]], particip35$`Code de la commune`)
  particip35[index_nc[i], 5] <- sum(particip35[index_temp,5]) #Somme des inscrits
  particip35[index_nc[i], 6] <- sum(particip35[index_temp,6]) #Somme des abstentions 
  particip35[index_nc[i], 8] <- sum(particip35[index_temp,8]) #Somme des votants 
  particip35[index_nc[i], 7] <- round(100*particip35[index_nc[i], 6]/particip35[index_nc[i], 5],2)#Calcul Abs/Ins
  particip35[index_nc[i], 9] <- round(100*particip35[index_nc[i], 8]/particip35[index_nc[i], 5],2)#Calcul Vot/Ins
}
particip35[index_nc, 4] <- c("Saint-Marc-le-Blanc" , "Piré-Chancé", "Luitré-Dompierre", "Val-Couesnon", "Mesnil-Roc'h", "Rives-du-Couesnon", "Montauban-de-Bretagne")
particip35 <- particip35[-index_cd,]
particip35 <- particip35[,-c(1,2)] #Osef info département
write.csv(particip35, file  = "participation_35.csv")

##Fusion des tables
particip35$`Code de la commune` <- particip35$`Code de la commune` + 35000
particip35$`Code de la commune` <- as.character(particip35$`Code de la commune`)
colnames(particip35)[1] <- "Code"
donnees <- merge(x = data, y = particip35)

#Problème accent trait d'union et "oe" entre les libellés (282 mdrr)
donnees <- donnees[,-44]
donnees[donnees$Libellé == "Maen Roch",2] <- "Maen-Roch"#Faute trait union
save(donnees, file = "data.Rda")

