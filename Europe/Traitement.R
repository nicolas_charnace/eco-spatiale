rm(list=ls())
library(sp)
library(spdep)
library(rgdal)
library(RColorBrewer)
library(classInt)
library(ggplot2)
library(ggmap)
library(maptools)
library(readr)
library(cartography)
library(readxl)

#####CARTE
ogrInfo(dsn = ".", layer = "DEPARTEMENT")
dep <- readOGR(dsn = ".", layer = "DEPARTEMENT")
head(dep@polygons, n=1)
plot(dep)
dep@data$INSEE_DEP <- as.character(dep@data$INSEE_DEP)
colnames(dep@data)[4] <- "Code"
save(dep, file = "departementsSANSDOM.Rda")

#####DONNEES
resultats <- read_excel("resultats-definitifs-par-departement.xls")
resultats_m <- resultats[-c(97:107),] #en france métropolitaine
save(resultats_m, file = "resultats_europeennes2019.Rda")
#F*ck Lyon 6-9 la trik
colnames(resultats_m)[1] <- "insee"
dep@data <- merge(dep@data, resultats_m, by = "insee")

donnees <- read_excel("data.xlsx", skip = 4)
resultats_m <- resultats_m[, c(1:16,47:51,172:177,221:226)]
colnames(resultats_m)[19:21] <-  c("LREM Voix", "LREM % Voix/Ins",	"LREM % Voix/Exp")
colnames(resultats_m)[25:27] <-  c("RN Voix", "RN % Voix/Ins",	"RN % Voix/Exp")
colnames(resultats_m)[31:33] <-  c("EELV Voix", "EELV % Voix/Ins",	"EELV % Voix/Exp")
resultats_m <- resultats_m[, -c(17,18,22,23,24,28,29,30)]
colnames(resultats_m)[1] <- "Code"
donnees <- merge(donnees, resultats_m)
names(donnees) <- str_replace(names(donnees), " ", "_")

donnees[, c(3, 18:24)] <- round(100*donnees[, c(3, 18:24)]/donnees[, 17], 2)
donnees <- donnees[, -17]
names(donnees) <- names(donnees) %>% str_replace("Population", "Part")

colnames(donnees)[c(3, 17:23)]<- c("Part_au_moins_15_ans_selon_la_CSP_Professions_intermédiaires",
                                   "Part_au_moins_15_ans_selon_la_CSP_Autres_personnes_sans_activité_professionnelle",   
                                   "Part_au_moins_15_ans_selon_la_CSP_Artisans,_commerçants,_chefs_entreprise",          
                                   "Part_au_moins_15_ans_selon_la_CSP_Agriculteurs_exploitants",                         
                                   "Part_au_moins_15_ans_selon_la_CSP_Cadres_et_professions_intellectuelles_supérieures",
                                   "Part_au_moins_15_ans_selon_la_CSP_Retraités",                                        
                                   "Part_au_moins_15_ans_selon_la_CSP_Ouvriers",
                                   "Part_au_moins_15_ans_selon_la_CSP_Employés")
save(donnees, file="data.Rda")

#test départements
for (i in 1:96){
  col.map <- rep(0,96)
  col.map[i] <- "red"
  plot(dep, col = col.map)
}

lisaRNleo <- read.table("~/Téléchargements/lisaRNleo.csv")
View(lisaRNleo)

vPal5 <- brewer.pal(n = 5, name = "Reds")

##merge entre la carte et la data

lJenks2013 <- classIntervals(var = dep@data$RN_pp_Voix_sur_Exp,
                             n = 5,
                             style = "jenks")
vJenks2013 <- lJenks2013$brks
dep@data$cho <- as.character(cut(dep@data$RN_pp_Voix_sur_Exp,
                                       breaks = vJenks2013,
                                       labels = vPal5,
                                       include.lowest = TRUE,
                                       right = FALSE))
vLegendBoxJ5 <- as.character(levels(cut(dep@data$RN_pp_Voix_sur_Exp,
                                        breaks = vJenks2013,
                                        include.lowest = TRUE,
                                        right = FALSE)))
plot(dep, col = dep@data$cho, border = "white")
legend("bottomleft",
       legend = vLegendBoxJ5,
       bty = "n",
       fill = vPal5,
       cex = 0.8,
       title = "Votes RN (% sur exprimés")
title(main = "Votes RN aux élections européennes 2019 par département")
