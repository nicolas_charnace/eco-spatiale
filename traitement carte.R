rm(list=ls())

#source de la carte : https://www.data.gouv.fr/fr/datasets/decoupage-administratif-communal-francais-issu-d-openstreetmap/
library(sp)
library(rgdal)
library(RColorBrewer)
library(classInt)
library(ggplot2)
library(ggmap)
library(maptools)
library(readr)


ogrInfo(dsn = ".", layer = "communes-20210101")
comm <- readOGR(dsn = ".", layer = "communes-20210101")
head(comm@polygons, n=1)
plot(comm)

#On ajoute le numéro du département
departement <- substr(comm@data$insee, 1, 2)
comm@data <- data.frame(comm@data, departement) 
head(comm@data)

#Carto des communes du 35
comm_35 <- comm[comm$departement == "35",]
head(comm_35@data)
plot(comm_35)
rm(comm)
save(comm_35, file = "communes35.Rda")